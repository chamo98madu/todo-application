<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/new', [
    'uses' => 'todosController@new'

]);

Route::get('/todos', [
    'uses' => 'todosController@index',
    'as' => 'todos'

]);

Route::get('/todos/delete/{id}', [
    'uses' => 'todosController@delete',
    'as' => 'todo.delete'

]);

Route::get('/todos/update/{id}', [
    'uses' => 'todosController@update',
    'as' => 'todo.update'

]);

Route::post('/todos/save/{id}', [
    'uses' => 'todosController@save',
    'as' => 'todo.save'

]);

Route::post('/create/todo', [
    'uses' => 'todosController@store'

]);

Route::get('/todos/completed/{id}', [
    'uses' => 'todosController@completed',
    'as' => 'todo.completed'

]);